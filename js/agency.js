/*!
 * Start Bootstrap - Agency Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});



$(document).ready(function(){
    
    $('#team1').mouseenter(function(){
        $('#teamF1').css("display","none");
        $('#teamF2').css("display","block");
        $('#teamH1').css("display","none");
        $('#teamH2').css("display","block");
    });
     $('#team1').mouseleave(function(){
        $('#teamF1').css("display","block");
        $('#teamF2').css("display","none");
          $('#teamH1').css("display","block");
        $('#teamH2').css("display","none");
    });
    
    $('#team2').mouseenter(function(){
        $('#teamF3').css("display","none");
        $('#teamF4').css("display","block");
        $('#teamH3').css("display","none");
        $('#teamH4').css("display","block");
    });
     $('#team2').mouseleave(function(){
        $('#teamF3').css("display","block");
        $('#teamF4').css("display","none");
         $('#teamH3').css("display","block");
        $('#teamH4').css("display","none");
    });
    
    $('#team3').mouseenter(function(){
        $('#teamF5').css("display","none");
        $('#teamF6').css("display","block");
        $('#teamH5').css("display","none");
        $('#teamH6').css("display","block");
    });
     $('#team3').mouseleave(function(){
        $('#teamF5').css("display","block");
        $('#teamF6').css("display","none");
        $('#teamH5').css("display","block");
        $('#teamH6').css("display","none");
    });
    
    $('#team4').mouseenter(function(){
        $('#teamF7').css("display","none");
        $('#teamF8').css("display","block");
        $('#teamH7').css("display","none");
        $('#teamH8').css("display","block");
    });
     $('#team4').mouseleave(function(){
        $('#teamF7').css("display","block");
        $('#teamF8').css("display","none");
         $('#teamH7').css("display","block");
        $('#teamH8').css("display","none");
    });
    
    
    
});



